ARG os_codename=stretch
ARG os_type=debian

FROM $os_type:$os_codename

LABEL maintainer="dcook@prosentient.com.au"

RUN apt-get update && \
    apt-get install -y \
        devscripts \
        wget \
        gnupg2 \
        bash-completion \
	equivs \
        sudo

ARG DEBIAN_FRONTEND=noninteractive

RUN apt-key adv --fetch-keys http://debian.koha-community.org/koha/gpg.asc

ARG apt_repo_codename=stable
RUN echo "deb http://debian.koha-community.org/koha $apt_repo_codename main" | tee /etc/apt/sources.list.d/koha.list
RUN echo "deb-src http://debian.koha-community.org/koha $apt_repo_codename main" | tee -a /etc/apt/sources.list.d/koha.list


#Create unprivileged koha user
RUN useradd -m koha
COPY files/koha.sudoers /etc/sudoers.d/koha
USER koha
WORKDIR /home/koha

#Create build directory
RUN mkdir /home/koha/build
RUN mkdir /home/koha/input
RUN mkdir /home/koha/output

