#!/bin/sh

BASE_DIR="/home/koha"

TARBALL=$1
RESULT_FILE_NAME=$2
RESULT_FILE="$BASE_DIR/output/$RESULT_FILE_NAME"

echo "TARBALL $TARBALL"
echo "RESULT_FILE $RESULT_FILE"

if [ ! "$RESULT_FILE" ] || [ ! -f "$RESULT_FILE" ]; then
    RESULT_FILE=$(mktemp $BASE_DIR/output/manifest.XXXXXXXXXX)
fi

if [ "$TARBALL" ]; then
    INPUT_TARBALL="$BASE_DIR/input/$TARBALL"
    if [ "$INPUT_TARBALL" ] && [ -f "$INPUT_TARBALL" ]; then
        FILE_TYPE=$(file -b --mime-type "$INPUT_TARBALL")
        if [ "$FILE_TYPE" ] && [ "$FILE_TYPE" = "application/gzip" ]; then
            BUILD_TARBALL="$BASE_DIR/build/$TARBALL"
            cp "$INPUT_TARBALL" "$BUILD_TARBALL"
            cd $BASE_DIR/build
            PERL_CMD='print /koha_(.*)\.orig\.tar\.gz$/;'
            echo "$BUILD_TARBALL"
            VERSION=$(echo "$BUILD_TARBALL" | perl -ne "$PERL_CMD")
            RELEASE_BASE=${TARBALL%.orig.tar.gz}
            echo $VERSION
            if [ "$VERSION" ]; then
                tar xzf "${BUILD_TARBALL}"
                cd "koha-${VERSION}"
                dch --force-distribution -D stable -v "$VERSION-1" "Building git snapshot"
                #FIXME: dch warning: Previous package version was Debian native whilst new version is not
                dch -r "Building git snapshot"
                #FIXME: Surely we don't need every Koha dependency listed as a build dependency...
                sudo apt-get update

		#Build koha-build-deps package in /tmp so that koha-build-deps*.buildinfo and koha-build-deps*.changes files don't interfere with build
		cd /tmp
                sudo mk-build-deps --tool='apt-get -y' --remove --install "$BASE_DIR/build/koha-${VERSION}/debian/control"
		cd -

                #FIXME: add --no-lintian to try to make the builder faster...
                DEB_BUILD_OPTIONS=nocheck debuild --no-lintian -us -uc

                cd $BASE_DIR/build
                #NOTE: Writing to result file makes automation easier
                echo "Writing results to $RESULT_FILE"
                #NOTE: In many cases, we may need to sudo to write to files in the output directory
                sudo truncate --size 0 "$RESULT_FILE"
                for file in koha*$VERSION-1_all.deb; do
                    if [ "$file" ] && [ -f "$file" ]; then
                        echo "$file"
                        sudo sh -c "echo $file >> $RESULT_FILE"
                        sudo mv "$file" $BASE_DIR/output/.
                    fi
                done

                #Cleanup unpacked tarball
                rm -r --interactive=never "$BASE_DIR/build/koha-$VERSION"
                #Cleanup Debian build output
                rm -r --interactive=never $BASE_DIR/build/$RELEASE_BASE*
            fi
        else
            echo "This file is not gzipped"
        fi 
    else
        echo "This file does not exist"
    fi
else
    echo "You must provide the name of a gzipped tarball for this script!"
fi
