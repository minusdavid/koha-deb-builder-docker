# koha-deb-builder-docker

This is a Dockerfile and a script for building Debian packages for Koha using a properly structured tarball. 

## Build Docker image
docker build -t koha-package-builder .

## Create Docker container

docker-compose up -d

## Package building

On Host:
1. cd /home/koha-build/kohaclone
2. git archive --format=tar --prefix="koha-UPSTREAM_VERSION+COMMENT.GITHASH/" HEAD | gzip -9 > "/home/koha-build/koha-deb-builder-docker/input/koha_UPSTREAM_VERSION+COMMENT.GITHASH.orig.tar.gz"
3. docker exec koha-package-builder scripts/build.sh "koha_UPSTREAM_VERSION+COMMENT.GITHASH.orig.tar.gz"
4. Collect Debian packages from /home/koha-build/koha-deb-builder-docker/output once the build script is finished running in the container

